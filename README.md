## 1. Install packages

```
npm install
```


## 2. Run project localy

```
npm run
```

---

## 3. Build project

Change package.json section "scripts:build" parameter "env.PUBLIC_DIR" for all relative URLs on a page (<base href="..."> )

```
npm run build
```

---

