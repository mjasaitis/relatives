import React, { Component } from "react";
import { connect } from "react-redux";
import { Alert } from "reactstrap";

class AlertPop extends Component {
	constructor(props) {
		super(props);
		this.onCloseClick = this.onCloseClick.bind(this);
		this.state = { hideTimeout: false, message: null, type: null };
	}

	onCloseClick() {
		this.close();
	}

	close() {
		this.setState({ message: null });
	}

	componentWillReceiveProps(nextProps) {
		clearTimeout(this.state.hideTimeout);

		const isVisible = this.state.message ? true : false;
		const showAlert = nextProps.alertPop.message ? true : false;

		if (showAlert) {
			const hideTimeout = setTimeout(() => {
				this.close();
			}, 4000);

			let hideDuration = 0;
			if (isVisible && this.state.message != nextProps.alertPop.message) {
				hideDuration = 250;
				this.close();
			}

			setTimeout(() => {
				this.setState({
					message: nextProps.alertPop.message,
					type: nextProps.alertPop.type
				});
			}, hideDuration);

			this.setState({ hideTimeout: hideTimeout });
		}
	}

	render() {
		const isVisible = this.state.message ? true : false;
		return (
			<Alert
				className="alert-fixed"
				color={this.state.type}
				isOpen={isVisible}
				toggle={this.onCloseClick}
			>
				{this.state.message}
			</Alert>
		);
	}
}

function mapStateToProps(state) {
	return { alertPop: state.alertPop };
}

export default connect(mapStateToProps)(AlertPop);
