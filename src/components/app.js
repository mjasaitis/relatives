import React, { Component } from "react";
import AlertPop from "./alertpop";
import PersonsList from "./persons/persons_list";
import PersonForm from "./persons/person_form";

class App extends Component {
	render() {
		return (
			<div>
				<AlertPop />
				<div className="container">
					<PersonsList />
					<PersonForm />
				</div>
			</div>
		);
	}
}

export default App;
