import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

import { fetchPersons, selectPerson } from "./../../actions/persons";

class PersonsList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			cellMapping: {
				Name: "name",
				"Last Name": "lname",
				Birthdate: "bdate"
			},
			orderBy: "name",
			orderByAsc: true
		};
	}

	componentDidMount() {
		this.props.fetchPersons();
	}

	onOrderByChange(orderName) {
		if (this.state.orderBy == orderName) {
			this.setState({ orderByAsc: !this.state.orderByAsc });
		} else {
			this.setState({ orderBy: orderName, orderByAsc: true });
		}
	}

	renderPersonsList() {
		return this.props.relatives
			.sort((a, b) => {
				const valA = a[this.state.orderBy];
				const valB = b[this.state.orderBy];
				if (this.state.orderByAsc) {
					return valA < valB ? -1 : valA > valB ? 1 : 0;
				} else {
					return valB < valA ? -1 : valB > valA ? 1 : 0;
				}
			})
			.map((person, index) => {
				return (
					<tr
						key={index}
						className="d-flex cursor-pointer"
						onClick={e => {
							this.props.selectPerson(person);
						}}
					>
						<td className="col-4">{person.name}</td>
						<td className="col-4">{person.lname}</td>
						<td className="col-4">{person.bdate}</td>
					</tr>
				);
			});
	}

	renderTableHeaderCell(name) {
		return (
			<th className="col-4">
				<a
					className="btn text-primary"
					onClick={e =>
						this.onOrderByChange(this.state.cellMapping[name])
					}
				>
					{name}{" "}
					{this.state.orderBy == this.state.cellMapping[name] ? (
						this.state.orderByAsc ? (
							<i className="fas fa-arrow-up" />
						) : (
							<i className="fas fa-arrow-down" />
						)
					) : (
						""
					)}
				</a>
			</th>
		);
	}

	render() {
		if (!this.props.relatives.length) {
			return null;
		}

		return (
			<div>
				<div className="table-responsive">
					<table className="table table-hover">
						<thead className="thead-light">
							<tr className="d-flex">
								{this.renderTableHeaderCell("Name")}
								{this.renderTableHeaderCell("Last Name")}
								{this.renderTableHeaderCell("Birthdate")}
							</tr>
						</thead>
						<tbody>{this.renderPersonsList()}</tbody>
					</table>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		relatives: state.persons.all
	};
}

export default connect(
	mapStateToProps,
	{ fetchPersons, selectPerson }
)(PersonsList);
