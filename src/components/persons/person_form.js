import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import DatePicker from "react-datepicker";
import moment from "moment";

import {
	savePerson,
	selectPerson,
	findRelatives,
	isNameValid,
	isLastNameValid
} from "./../../actions/persons";

class PersonForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			modalVisible: false,
			name: "",
			isNameValid: false,
			lname: "",
			isLastNameValid: false,
			bdate: null
		};

		this.closeModal = this.closeModal.bind(this);
		this.onNameChange = this.onNameChange.bind(this);
		this.onLastNameChange = this.onLastNameChange.bind(this);
		this.onBirthDateChange = this.onBirthDateChange.bind(this);
		this.savePerson = this.savePerson.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (
			nextProps.person &&
			(!this.props.person ||
				(this.props.person &&
					this.props.person.id != nextProps.person.id))
		) {
			this.setState(
				{
					modalVisible: true,
					...nextProps.person,
					bdate: moment(nextProps.person.bdate)
				},
				() => {
					this.checkFormFields();
				}
			);

			this.props.findRelatives(nextProps.person);
		}
	}

	closeModal() {
		this.setState({ modalVisible: false });
		this.props.selectPerson(null);
	}

	checkName() {
		this.setState({
			isNameValid: isNameValid(this.state.name)
		});
	}

	checkLastName() {
		this.setState({
			isLastNameValid: isLastNameValid(this.state.lname)
		});
	}

	checkFormFields() {
		this.checkName();
		this.checkLastName();
	}

	isFormValid() {
		return this.state.isNameValid && this.state.isLastNameValid;
	}

	onNameChange(e) {
		this.setState({ name: e.target.value }, () => {
			this.checkName();
		});
	}

	onLastNameChange(e) {
		this.setState({ lname: e.target.value }, () => {
			this.checkLastName();
		});
	}

	onBirthDateChange(date) {
		this.setState({ bdate: date });
	}

	savePerson() {
		this.props.savePerson({
			id: this.state.id,
			name: this.state.name,
			lname: this.state.lname,
			bdate: this.state.bdate
		});

		this.closeModal();
	}

	renderRelatives() {
		return this.props.relatives.map((person, index) => {
			return (
				<tr key={index}>
					<td className="small">{person.name}</td>
					<td className="small">{person.lname}</td>
					<td className="small">{person.bdate}</td>
					<td className="small">{person.relation}</td>
				</tr>
			);
		});
	}

	render() {
		return (
			<Modal isOpen={this.state.modalVisible} toggle={this.closeModal}>
				<ModalHeader toggle={this.toggle}>Edit person</ModalHeader>
				<ModalBody>
					<div className="form-group row">
						<label className="col-3 col-form-label">Name:</label>
						<div className="col-9">
							<input
								type="text"
								className={
									this.state.isNameValid
										? "form-control"
										: "form-control is-invalid"
								}
								value={this.state.name}
								onChange={this.onNameChange}
								maxLength="50"
							/>
						</div>
					</div>
					<div className="form-group row">
						<label className="col-3 col-form-label">
							Last name:
						</label>
						<div className="col-9">
							<input
								type="text"
								className={
									this.state.isLastNameValid
										? "form-control"
										: "form-control is-invalid"
								}
								value={this.state.lname}
								onChange={this.onLastNameChange}
								maxLength="50"
							/>
						</div>
					</div>
					<div className="form-group row">
						<label className="col-3 col-form-label">
							Birth date:
						</label>
						<div className="col-9">
							<DatePicker
								className="form-control"
								selected={this.state.bdate}
								onChange={this.onBirthDateChange}
								dateFormat="YYYY-MM-DD"
							/>
						</div>
					</div>
				</ModalBody>
				<ModalFooter>
					<Button
						color="primary"
						disabled={!this.isFormValid()}
						onClick={this.closePasswordFormAndSave}
						onClick={this.savePerson}
					>
						Save
					</Button>
				</ModalFooter>
				<ModalHeader>Relatives:</ModalHeader>
				<ModalBody>
					{this.props.relatives.length ? (
						<table className="table table-hover table-sm table-border-bottom">
							<tbody>{this.renderRelatives()}</tbody>
						</table>
					) : (
						<span>No relatives found</span>
					)}
				</ModalBody>
			</Modal>
		);
	}
}

function mapStateToProps(state) {
	return {
		person: state.persons.selectedPerson,
		relatives: state.persons.relatives
	};
}

export default connect(
	mapStateToProps,
	{ savePerson, selectPerson, findRelatives }
)(PersonForm);
