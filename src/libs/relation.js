import moment from "moment";

export class Relation {
	ageDiffSiblings = 15;
	ageDiffParents = [16, 40];
	ageDiffGrandParents = 41;

	constructor(person) {
		this.person1 = person;
	}

	getYearsDiff(person1, person2) {
		const duration = moment.duration(
			moment(person2.bdate).diff(moment(person1.bdate))
		);
		const years = Math.floor(duration.asYears());
		return years;
	}

	findSimilarLastNames(person1, person2) {
		let similar = false;

		person1.lnames.forEach(person1Lname => {
			person2.lnames.forEach(person2Lname => {
				if (person1Lname.root == person2Lname.root) {
					similar = [person1Lname, person2Lname];
				}
			});
		});

		return similar;
	}

	setComparePerson(person) {
		this.person2 = person;
	}

	isYounger() {
		return this.yearsDiff > 0 ? true : false;
	}

	isOlder() {
		return !this.isYounger();
	}

	isYearsDiffInSiblingsRange() {
		return Math.abs(this.yearsDiff) <= this.ageDiffSiblings ? true : false;
	}

	isYearsDiffInParentsRange() {
		const yearsDiff = Math.abs(this.yearsDiff);
		return yearsDiff >= this.ageDiffParents[0] &&
			yearsDiff <= this.ageDiffParents[1]
			? true
			: false;
	}

	isYearsDiffInGransParentsRange() {
		return Math.abs(this.yearsDiff) >= this.ageDiffGrandParents
			? true
			: false;
	}

	isFather() {
		if (this.person2.gender != "m") {
			return;
		}

		if (!(this.isYearsDiffInParentsRange() && this.isOlder())) {
			return false;
		}

		if (this.person1.gender == "f") {
			if (!this.person1.similarLastName.married) {
				return true;
			}
		} else {
			return true;
		}

		return false;
	}

	isMother() {
		if (this.person2.gender != "f") {
			return;
		}

		if (!(this.isYearsDiffInParentsRange() && this.isOlder())) {
			return false;
		}

		if (
			!this.person1.similarLastName.married &&
			this.person2.similarLastName.married
		) {
			return true;
		} else {
			return false;
		}
	}

	isBrother() {
		if (this.person2.gender != "m") {
			return;
		}

		if (!this.isYearsDiffInSiblingsRange()) {
			return false;
		}

		if (this.person1.gender == "m") {
			return true;
		} else {
			if (!this.person1.similarLastName.married) {
				return true;
			}
		}
	}

	isSister() {
		if (this.person2.gender != "f") {
			return;
		}

		if (!this.isYearsDiffInSiblingsRange()) {
			return false;
		}

		if (this.person1.gender == "m") {
			if (!this.person2.similarLastName.married) {
				return true;
			}
		} else {
			if (
				!this.person1.similarLastName.married &&
				!this.person2.similarLastName.married
			) {
				return true;
			}
		}
	}

	isHusband() {
		if (this.person1.gender != "f" || this.person2.gender != "m") {
			return;
		}

		if (!this.isYearsDiffInSiblingsRange()) {
			return false;
		}

		if (this.person1.similarLastName.married) {
			return true;
		}
	}

	isWife() {
		if (this.person1.gender != "m" || this.person2.gender != "f") {
			return;
		}

		if (!this.isYearsDiffInSiblingsRange()) {
			return false;
		}

		if (this.person2.similarLastName.married) {
			return true;
		}
	}

	isGrandFather() {
		if (this.person2.gender != "m") {
			return;
		}

		if (!(this.isYearsDiffInGransParentsRange() && this.isOlder())) {
			return false;
		}

		if (this.person1.gender == "f") {
			if (!this.person1.similarLastName.married) {
				return true;
			}
		} else {
			return true;
		}

		return false;
	}

	isGrandMother() {
		if (this.person2.gender != "f") {
			return;
		}

		if (!(this.isYearsDiffInGransParentsRange() && this.isOlder())) {
			return false;
		}

		if (this.person1.gender == "f") {
			if (!this.person1.similarLastName.married) {
				return true;
			}
		} else {
			return true;
		}

		return false;
	}

	isGrandSon() {
		if (this.person2.gender != "m") {
			return;
		}

		if (!(this.isYearsDiffInGransParentsRange() && this.isYounger())) {
			return false;
		}

		if (this.person1.gender == "f") {
			if (this.person1.similarLastName.married) {
				return true;
			}
		} else {
			return true;
		}

		return false;
	}

	isGrandDaughter() {
		if (
			this.person2.gender != "f" ||
			this.person2.similarLastName.married
		) {
			return;
		}

		if (!(this.isYearsDiffInGransParentsRange() && this.isYounger())) {
			return false;
		}

		if (this.person1.gender == "f") {
			if (this.person1.similarLastName.married) {
				return true;
			}
		} else {
			return true;
		}

		return false;
	}

	isDaughter() {
		if (
			this.person2.gender != "f" ||
			this.person2.similarLastName.married
		) {
			return;
		}

		if (!(this.isYearsDiffInParentsRange() && this.isYounger())) {
			return false;
		}

		if (this.person1.gender == "f") {
			if (this.person1.similarLastName.married) {
				return true;
			}
		} else {
			return true;
		}

		return false;
	}

	isSon() {
		if (this.person2.gender != "m") {
			return;
		}

		if (!(this.isYearsDiffInParentsRange() && this.isYounger())) {
			return false;
		}

		if (this.person1.gender == "f") {
			if (this.person1.similarLastName.married) {
				return true;
			}
		} else {
			return true;
		}

		return false;
	}

	findRelation() {
		this.yearsDiff = this.getYearsDiff(this.person1, this.person2);
		const similarLastNames = this.findSimilarLastNames(
			this.person1,
			this.person2
		);

		if (!similarLastNames) {
			return false;
		}

		this.person1.similarLastName = similarLastNames[0];
		this.person2.similarLastName = similarLastNames[1];

		if (this.isFather()) {
			return "father";
		} else if (this.isMother()) {
			return "mother";
		} else if (this.isBrother()) {
			return "brother";
		} else if (this.isSister()) {
			return "sister";
		} else if (this.isHusband()) {
			return "husband";
		} else if (this.isWife()) {
			return "wife";
		} else if (this.isGrandFather()) {
			return "grand father";
		} else if (this.isGrandMother()) {
			return "grand mother";
		} else if (this.isGrandSon()) {
			return "grand sun";
		} else if (this.isGrandDaughter()) {
			return "grand daughter";
		} else if (this.isDaughter()) {
			return "daughter";
		} else if (this.isSon()) {
			return "son";
		}
	}
}
