export const ALERTPOP_SHOW = "ALERTPOP_SHOW";
export const ALERTPOP_HIDE = "ALERTPOP_HIDE";

export function showSuccessAlert(dispatch, message) {
	dispatch({
		type: ALERTPOP_SHOW,
		payload: {
			type: "success",
			message: message
		}
	});
}

export function showErrorAlert(dispatch, err) {
	dispatch({
		type: ALERTPOP_SHOW,
		payload: {
			type: "danger",
			message:
				err.response && err.response.data.error
					? err.response.data.error
					: err.toString()
		}
	});
}
