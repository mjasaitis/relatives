import axios from "axios";
import { ALERTPOP_SHOW, showSuccessAlert, showErrorAlert } from "./alertpop";

export const PERSONS_FETCH = "PERSONS_FETCH";
export const PERSON_SELECT = "PERSON_SELECT";
export const PERSON_SAVE = "PERSON_SAVE";
export const PERSON_FIND_RELATIVES = "PERSON_FIND_RELATIVES";

export function fetchPersons() {
	const request = axios({
		method: "get",
		url: `${process.env.PUBLIC_DIR}data/persons.json`,
		responseType: "json"
	});

	return dispatch => {
		request
			.then(response => {
				dispatch({ type: PERSONS_FETCH, payload: response.data });
				dispatch({ type: ALERTPOP_SHOW, payload: {} });
			})
			.catch(error => {
				showErrorAlert(dispatch, "Cannot fetch data.");
			});
	};
}

export function selectPerson(person) {
	return { type: PERSON_SELECT, payload: person };
}

export function savePerson(person) {
	return dispatch => {
		dispatch({ type: PERSON_SAVE, payload: person });
		showSuccessAlert(dispatch, "Person saved.");
	};
}

export function findRelatives(person) {
	return { type: PERSON_FIND_RELATIVES, payload: person };
}

export function isNameValid(value) {
	return /^([A-ZĄČĘĖĮŠŲŪŽ]{1}[a-ząčęėįšųūž]{1,})(\s[A-ZĄČĘĖĮŠŲŪŽ]{1}[a-ząčęėįšųūž]{1,})?$/.test(
		value
	);
}

export function isLastNameValid(value) {
	let valid = /^([A-ZĄČĘĖĮŠŲŪŽ][a-ząčęėįšųūž]{1,}(s|ė))(-[A-ZĄČĘĖĮŠŲŪŽ][a-ząčęėįšųūž]{1,}(s|ė))?$/.test(
		value
	);

	let lastNameParts = value.split("-");

	if (typeof lastNameParts[1] != "undefined") {
		const lastLetter0 = lastLetter(lastNameParts[0]);
		const lastLetter1 = lastLetter(lastNameParts[1]);
		if (lastLetter0 != lastLetter1) {
			valid = false;
		}
	}

	return valid;
}

function lastLetter(value) {
	return value.substr(value.length - 1);
}
