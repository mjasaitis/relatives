import { ALERTPOP_SHOW, ALERTPOP_HIDE } from "./../actions/alertpop";

const initialState = {
    type: null,
    message: null
};

export default function(state = initialState, action) {
    switch (action.type) {
        case ALERTPOP_SHOW:
            return {
                type: action.payload.type,
                message: action.payload.message
            };
        case ALERTPOP_HIDE:
            return initialState;
        default:
            return state;
    }
}
