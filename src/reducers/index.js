import { combineReducers } from "redux";
import { reducer as reducerForm } from "redux-form";

import reducerPersons from "./reducer_persons";
import reducerAlertPop from "./reducer_alertpop";

const rootReducer = combineReducers({
	alertPop: reducerAlertPop,
	persons: reducerPersons
});

export default rootReducer;
