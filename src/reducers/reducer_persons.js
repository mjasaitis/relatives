import {
	PERSONS_FETCH,
	PERSON_SELECT,
	PERSON_SAVE,
	PERSON_FIND_RELATIVES
} from "../actions/persons";
import { Relation } from "./../libs/relation";

const INITIAL_STATE = { all: [], selectedPerson: null, relatives: [] };

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case PERSONS_FETCH:
			return { ...state, all: action.payload };
		case PERSON_SELECT:
			return { ...state, selectedPerson: action.payload };
		case PERSON_FIND_RELATIVES:
			const relatives = findRelatives(action.payload, state.all);
			return { ...state, relatives: relatives };
		case PERSON_SAVE:
			const index = state.all.findIndex(
				elem => elem.id == action.payload.id
			);

			if (index != -1) {
				let persons = [...state.all];
				persons[index] = action.payload;
				persons[index].bdate = persons[index].bdate.format(
					"YYYY-MM-DD"
				);
				return { ...state, selectedPerson: null, all: persons };
			} else {
				return { ...state, selectedPerson: null };
			}

		default:
			return state;
	}
}

function findRelatives(person, list) {
	let relatives = [];
	let relationOb;

	person = setAdditionalPersonInfo(person);
	relationOb = new Relation(person);

	list.forEach(otherPerson => {
		if (person.id != otherPerson.id) {
			otherPerson = setAdditionalPersonInfo(otherPerson);

			relationOb.setComparePerson(otherPerson);
			let relation = relationOb.findRelation();

			if (relation) {
				relatives.push({
					...otherPerson,
					relation: relation
				});
			}
		}
	});

	return relatives;
}

function setAdditionalPersonInfo(person) {
	person.gender = getGender(person.lname);
	const lastNames = person.lname.split("-");

	person.lnames = [];
	lastNames.forEach(lname => {
		let lnameRoot;
		if (person.gender == "f") {
			let married = /(.{1,}(ienė))$/.test(lname);

			lnameRoot = lname.replace(
				/(.{1,})(ytė|aitė|utė|ūtė|ienė)$/,
				"$1",
				""
			);

			person.lnames.push({ root: lnameRoot, married: married });
		} else {
			lnameRoot = lname.replace(/(.{1,})(us|as|is|ys)$/, "$1", "");
			person.lnames.push({ root: lnameRoot });
		}
	});
	return person;
}

function getGender(value) {
	return value.substr(value.length - 1) == "s" ? "m" : "f";
}
