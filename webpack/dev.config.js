const webpack = require("webpack");
const merge = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const baseConfig = require("./base.config.js");
const path = require("path");

module.exports = env => {
    return merge(baseConfig, {
        output: {
            filename: "js/bundle.js",
            path: path.join(__dirname, "../public/"),
            publicPath: "/"
        },

        devServer: {
            inline: true,
            port: 8085,
            hot: true,
            historyApiFallback: true,
            contentBase: path.join(__dirname, "../public/"),
            publicPath: "/"
        },

        plugins: [
            new webpack.DefinePlugin({
                "process.env": {
                    PUBLIC_DIR: JSON.stringify(env.PUBLIC_DIR)
                }
            }),
            new HtmlWebpackPlugin({
                template: path.join(__dirname, "../src/html/index.html"),
                filename: path.join(__dirname, "../public/index.html"),
                inject: true,
                publicDir: env.PUBLIC_DIR
            })
        ]
    });
};
