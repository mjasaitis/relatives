const webpack = require("webpack");
const merge = require("webpack-merge");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const baseConfig = require("./base.config.js");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = env => {
	return merge(baseConfig, {
		output: {
			path: path.join(__dirname, "../public/"),
			filename: "js/[name].[chunkhash].bundle.js",
			chunkFilename: "js/[name].[chunkhash].bundle.js"
		},

		optimization: {
			splitChunks: {
				cacheGroups: {
					vendor: {
						test: /node_modules/,
						chunks: "initial",
						name: "vendor",
						enforce: true
					}
				}
			}
		},

		plugins: [
			new UglifyJsPlugin({
				sourceMap: false,
				uglifyOptions: {
					output: {
						comments: false
					}
				}
			}),
			new webpack.DefinePlugin({
				"process.env": {
					PUBLIC_DIR: JSON.stringify(env.PUBLIC_DIR)
				}
			}),
			new HtmlWebpackPlugin({
				template: path.join(__dirname, "../src/html/index.html"),
				inject: true,
				publicDir: env.PUBLIC_DIR
			})
		]
	});
};
