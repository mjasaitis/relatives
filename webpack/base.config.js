const webpack = require("webpack");

module.exports = {
    entry: ["./src/index.js"],
    module: {
        rules: [
            {
                exclude: /node_modules/,
                test: /.(js|jsx)$/,
                use: ['babel-loader']
            }
        ]
    },
    resolve: {
        extensions: [".js", ".jsx"]
    }
};
